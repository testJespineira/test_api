from app import app, mongo
import currencylayer
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import jsonify, request


#Lista todas las cervezas que se encuentran en la base de datos
@app.route('/beers',methods=['GET'])
def searchBeers():
	beer = mongo.db.beer.find()
	resp = dumps(beer)
	resp.status_code = 200
	return resp

#Ingresa una nueva cerveza
@app.route('/beers', methods=['POST'])
def addBeers():
	_json = request.json
	_id = _json['id']
	beer = mongo.db.beer.find_one({'id': ObjectId(_id)})
	if not beer:
		_name = _json['name']
		_brewery = _json['brewery']
		_country = _json['country']
		_price = _json['price']
		_currency = _json['currency']
		# valida atributos obligatorios
		if _name and _brewery and _country and _price and _currency and request.method == 'POST':
			# nuevo registro de cerveza en  db
			id = mongo.db.beer.insert_one({'id': _id,'name': _name, 'brewery': _brewery, 'country': _country , 'price': _price , 'currency': _currency})
			resp = jsonify('Nueva cerveza ingresada:' + id)
			resp.status_code = 201
			return resp
		else:
			return not_acceptable()
	else:
		return conflict()	


#Lista el detalle de la marca de cervezas		
@app.route('/beers/{beerID}',methods=['GET'])
def searchBeerById(beerID):
	beer = mongo.db.beer.find_one({'id': ObjectId(beerID)})
	if beer:
		resp = dumps(beer)
		resp.status_code = 200
		return resp
	else:
		return not_found()

#Lista el precio de una caja de cervezas de una marca
@app.route('/beers/{beerID}/boxprice',methods=['GET'])
def boxBeerPriceById(beerID):
	#Verifica que exista la beer
	beer = mongo.db.beer.find_one({'id': ObjectId(beerID)})
	if beer:
		#obtiene la moneda en que se desea  obtener el valor de la caja
		#Si no se expresa la moneda, entrega en moneda actual registrada del producto
		_to_currency=request.args.get('currency')
		if not _to_currency:
			_to_currency=beer['currency']

		#obtiene el numero de contenido de la caja, default 6
		_quantity = request.args.get('quantity')
		if not _quantity:
			_quantity=6

		#Conversion solo si monedas son distintas
		if _to_currency and _quantity and _to_currency!=_quantity:
			exchange_rate = currencylayer.Client(access_key='87dae8864154df9cfe56355369ae6f5f')
			exchange_rate.convert(from_currency=_to_currency, to_currency=_to_currency, amount=_quantity)
			json = response.json()
			currency_quantity = float(json['result'])
			# se obtiene valor total (nomeda cantidad precio)
			total = currency_quantity * ['price']
			resp = dumps(total)
			resp.status_code = 200
			return resp
		else:
			#Solo precio por cantidad
			total = _quantity * ['price']
			resp = dumps(total)
			return resp	
	else:
		return not_found()

		
@app.errorhandler(400)
def not_found(error=None):
    message = {
        'status': 400,
        'message': 'Request invalida ',
    }
    resp = jsonify(message)
    resp.status_code = 400

    return resp

if __name__ == "__main__":
    app.run()